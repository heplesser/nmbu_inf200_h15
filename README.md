# INF200 Advanced Programming

Welcome to INF200 *Advanced Programming* at NMBU in the fall and winter term 2015/16. This Bitbucket repository will provide source code and other material used in the course.

The [Wiki for the course](https://bitbucket.org/heplesser/nmbu_inf200_h15/wiki) will be the main forum for all matters pertaining to the course. The Fronter page for the course will **not** be used.
