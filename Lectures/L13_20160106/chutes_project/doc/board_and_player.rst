Board and Player
================

The board module
----------------
.. automodule:: chutes.board
   :members:

The player module
-----------------
.. automodule:: chutes.player
   :members:
