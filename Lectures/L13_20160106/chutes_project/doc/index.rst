.. Chutes documentation master file, created by
   sphinx-quickstart on Fri Jan  8 00:29:22 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Chutes's documentation!
==================================

This is a simulation of a Chutes & Ladders game with
 * a modifiable game board
 * several types of players

.. toctree::
   :maxdepth: 2

   simulation
   board_and_player


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

