# -*- coding: utf-8 -*-

"""
Tests for Board class.
"""

import nose.tools as nt
from ..board import Board

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class TestEmptyBoard(object):
    """
    Collects test that operate on a board without chutes and ladders.
    """

    def setup(self):
        """
        Executed before each test in class to prepare for test.
        """

        self.goal_pos = 20
        self.brd = Board(ladders=[], chutes=[], goal=self.goal_pos)

    def teardown(self):
        """
        Executed after each test in class to clean up.
        """

        pass   # nothing to clean up

    def test_goal_not_reached(self):
        """"Ensure goal_reached() does not yield false positives."""
        for pos in range(self.goal_pos):
            nt.assert_false(self.brd.goal_reached(pos))


    def test_goal_reached(self):
        """Ensure goal_reached() does not yield false negatives."""
        for pos in range(self.goal_pos, self.goal_pos+10):
            nt.assert_true(self.brd.goal_reached(pos))


    def test_adjust_empty_board(self):
        """"No position adjustment on empty board."""

        for pos in range(self.goal_pos):
            nt.assert_equal(self.brd.position_adjustment(pos), 0)
    

def test_adjustment():
    goal_pos = 20
    ladders = [(2, 10), (9, 13), (12, 18)]
    chutes = [(4, 1), (7, 3), (17, 8)]
    test_cases = {0: 0, 1: 0, 2: 8, 3: 0, 4: -3, 5: 0, 6: 0, 7: -4,
                  8: 0, 9: 4, 10: 0, 11: 0, 12: 6, 13: 0, 14: 0,
                  15: 0, 16: 0, 17: -9, 18: 0, 19: 0}
    brd = Board(ladders=ladders, chutes=chutes, goal=goal_pos)
    for pos, change in test_cases.items():
        nt.assert_equal(brd.position_adjustment(pos), change)


def test_default_board():
    """Some tests on default board."""

    brd = Board()
    nt.assert_equal(brd.position_adjustment(1), 39)
    nt.assert_equal(brd.position_adjustment(2), 0)
    nt.assert_equal(brd.position_adjustment(33), -30)
    nt.assert_false(brd.goal_reached(89))
    nt.assert_true(brd.goal_reached(90))
    nt.assert_true(brd.goal_reached(91))


def test_bad_boards():
    """Test that bad board specifications are not accepted."""

    nt.assert_raises(ValueError, Board, ladders=[(10, 10)])
    nt.assert_raises(ValueError, Board, ladders=[(10, 9)])
    nt.assert_raises(ValueError, Board, chutes=[(10, 10)])
    nt.assert_raises(ValueError, Board, chutes=[(10, 12)])
    nt.assert_raises(ValueError, Board, goal=0)
