# -*- coding: utf-8 -*-

"""
Tests for functions defined in module helpers.
"""

import nose.tools as nt
from ..helpers import type_to_name

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Foo(object):
    """
    Test class to test correct handling of objects.
    """

    pass


class FooFoo(Foo):
    pass


def test_integer():
    nt.assert_equal(type_to_name(10), 10)


def test_object():
    nt.assert_equal(type_to_name(Foo), 'Foo')


def test_list():
    nt.assert_equal(type_to_name([Foo, Foo, FooFoo]),
                    ['Foo', 'Foo', 'FooFoo'])


def test_tuple():
    nt.assert_equal(type_to_name((Foo, Foo, FooFoo)),
                    ('Foo', 'Foo', 'FooFoo'))


def test_dict():
    nt.assert_equal(type_to_name({Foo: 1, 2: FooFoo}),
                    {'Foo': 1, 2: 'FooFoo'})


def test_nested_dict():
    nt.assert_equal(type_to_name({Foo: 1,
                                  2: [(Foo, FooFoo), {Foo: 3}]}),
                    {'Foo': 1,
                     2: [('Foo', 'FooFoo'), {'Foo': 3}]})
