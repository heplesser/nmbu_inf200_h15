# -*- coding: utf-8 -*-

"""
Some very simple functions for illustrating the testing of
code using random numbers.
"""

import random

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def bernoulli_experiment(p, n):
    """
    Returns number of successes of Bernoulli experiment.

    :param p: Probability of success
    :param n: Number of trials
    :return: Number of successful trials
    """

    return sum(random.random() < p for _ in range(n))


def gaussian_data(mu, sigma, n):
    """
    Returns list of n normally distributed random numbers.

    :param mu: mean
    :param sigma: standard deviation
    :param n: number of numbers to generate
    :return: list of n numbers
    """

    return [random.gauss(mu, sigma) for _ in range(n)]


class Item(object):

    params = {'p_split': 0.2}

    def __init__(self):
        """
        Create new Item with counter initialized to 0.
        """
        self.counter = 0

    @classmethod
    def update_p_split(cls, new_p_split):
        """
        Update splitting probability for Item class.

        :param new_p_split: New probability [0, 1]
        """
        if not (0 <= new_p_split <= 1):
            raise ValueError('0 <= p_split <= 1 required.')
        cls.params['p_split'] = new_p_split

    def hike_counter(self):
        """
        Increase item state counter by 1.
        """
        self.counter += 1

    def splits(self):
        """
        Returns True if item splits.
        """
        return random.random() < self.params['p_split']


class Container(object):

    def __init__(self):
        """
        Create container without items.
        """
        self.items = []

    def add_items(self, items):
        """
        Add items to container

        :param items: Iterable containing items.
        """
        self.items.extend(items)

    def new_round(self):
        """
        Update state of items in container.
        """
        for item in self.items:
            item.hike_counter()

    def splitting(self):
        """
        Add new items for all those items that split.
        """
        new_items = [Item() for item in self.items if item.splits()]
        self.items.extend(new_items)
