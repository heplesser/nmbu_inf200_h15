# -*- coding: utf-8 -*-

"""
Illustrative statistical tests of sims code.
"""

import nose.tools as nt
import random
import scipy.stats
import math
import plmock

from .sims import bernoulli_experiment
from .sims import gaussian_data
from .sims import Item
from .sims import Container

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


# noinspection PyMethodMayBeStatic
class TestGaussian(object):

    SEED = 1234567
    ALPHA = 0.01

    def setup(self):
        random.seed(TestGaussian.SEED)

    def test_zero_sigma(self):
        mu = 10
        n = 10
        res = gaussian_data(mu, 0, n)
        nt.assert_equal(res, [mu] * n)

    def test_ztest(self):
        """
        Perform "manual" Z-test on mean
        """

        mu = 2.
        sigma = 3.
        n = 100

        mean = sum(gaussian_data(mu, sigma, n)) / float(n)

        std_err = sigma / math.sqrt(n)
        z_score = (mean - mu) / std_err
        p_value = 2 * scipy.stats.norm.cdf(-abs(z_score))

        nt.assert_greater(p_value, TestGaussian.ALPHA)

    def test_normal_distribution(self):
        """
        Test for normal distribution using SciPy's normality test.

        This test also illustrates the use of test generators.
        """

        n = 100
        for mu in [-10, -5, -.123, 0, 1, 2.345]:
            for sigma in [0.1, 1., 2.5]:
                yield TestGaussian._norm_dist_check, mu, sigma, n

    @staticmethod
    def _norm_dist_check(mu, sigma, n):
        data = gaussian_data(mu, sigma, n)
        _, p_value = scipy.stats.normaltest(data)

        nt.assert_greater(p_value, TestGaussian.ALPHA)


# noinspection PyMethodMayBeStatic
class TestBernoulli(object):

    SEED = 1234567
    ALPHA = 0.01

    def setup(self):
        random.seed(TestBernoulli.SEED)

    def test_zero(self):
        n = 1000
        nt.assert_equal(bernoulli_experiment(0, n), 0,
                        'Success for p == 0')

    def test_one(self):
        n = 1000
        nt.assert_equal(bernoulli_experiment(1, n), n,
                        'Failure for p == 1')

    def test_ztest(self):
        """
        Manual Z-test exploiting central limit theorem.
        """

        n = 1000
        p = 0.4

        n_success = bernoulli_experiment(p, n)

        mu = n * p
        sigma = math.sqrt(n * p * (1-p))

        z_score = (n_success - mu) / sigma
        p_value = 2 * scipy.stats.norm.cdf(-abs(z_score))

        nt.assert_greater(p_value, TestBernoulli.ALPHA)

    def test_binomial_test(self):
        """
        Binomial test from SciPy.
        """

        n = 1000
        p = 0.4

        n_success = bernoulli_experiment(p, n)

        p_value = scipy.stats.binom_test(n_success, n, p)

        nt.assert_greater(p_value, TestBernoulli.ALPHA)


class TestItem(object):

    def setup(self):
        self.item_params = Item.params.copy()
        self.orig_random = random.random

    def teardown(self):
        random.random = self.orig_random
        Item.params = self.item_params

    def test_hike_counter(self):
        item = Item()
        nt.assert_equal(item.counter, 0)
        for n in range(10):
            item.hike_counter()
            nt.assert_equal(item.counter, n+1)

    def test_splits(self):
        """
        Test that splitting works correctly.

        This test replaces the random number generator with a mock
        generator with predefined random numbers. We thus know the
        expected result.
        """

        # Use power of two to avoid rounding issues
        p_split = 2. ** -2
        Item.update_p_split(p_split)

        # Set up random numbers and expected result
        rand_and_result = ((0., True),
                           (p_split - 2.**-7, True),
                           (p_split, False),
                           (p_split + 2.**-7, False),
                           (1., False))
        random_numbers, expected_results = zip(*rand_and_result)

        # Replace random.random with Mock function
        random.random = plmock.FuncReturningValueSequence(random_numbers)

        # Create item and call splits() once per test case
        item = Item()
        results = [item.splits() for _ in rand_and_result]
        nt.assert_equal(results, list(expected_results))


class TestContainer(object):

    def setup(self):
        """
        Preserve methods that will be mocked.
        """

        self.orig_item_hike_counter = Item.hike_counter
        self.orig_item_splits = Item.splits

    def teardown(self):
        """
        Restore methods that may have been mocked.
        """

        Item.splits = self.orig_item_splits
        Item.hike_counter = self.orig_item_hike_counter

    def test_empty_container(self):
        c = Container()
        c.new_round()
        c.splitting()
        nt.assert_equal(c.items, [])

    def test_new_round(self):
        """
        Test that new round hikes counters.

        This test does not look at what actually happens inside
        the items, it only checkes that the hike counter method
        is called.
        """

        hc_mocker = plmock.FixedValueMethodMocker()
        Item.hike_counter = hc_mocker.get_method()

        c = Container()
        n_items = 10
        c.add_items([Item() for _ in range(n_items)])

        c.new_round()

        # Assert that hike_counter has been called exactly once per item
        nt.assert_equal(hc_mocker.num_calls(), n_items)
        nt.assert_false(hc_mocker.multiple_calls_per_caller())
