# Download instructions

In the exercise session on Friday, you will learn how to clone the course repository and pull updates. Until then, you can download the files here as follows:

1. Click on the PDF file with the lecture slides, then click "View raw".
1. Click the `Klokkekabal` folder and inside it `klokkekabal.py`. Then choose "Raw", mark all code shown, and paste it into a new Python file in PyCharm.

**Note**: The code for the `klokkekabal` program is now in the `ProgrammingTasks` directory.
