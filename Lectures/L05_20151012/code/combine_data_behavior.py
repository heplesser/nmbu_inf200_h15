# -*- coding: utf-8 -*-

import math

"""
Illustrate Idea 1: Combining data and behavior
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Circle(object):
    def __init__(self, center, radius):
        self.ctr = center
        self.rad = radius

    def area(self):
        return 2 * math.pi * self.rad**2


class Rectangle(object):
    def __init__(self, lower_left, upper_right):
        self.l_l = lower_left
        self.u_r = upper_right
        
    def area(self):    
        return (self.u_r[0] - self.l_l[0]) * (self.u_r[1] - self.l_l[1])


if __name__ == '__main__':
    
    shapes = [Circle((0, 0), 10), Circle((1, 1), 5),
              Rectangle((0.5, 0.5), (3, 2))]
    
    for shape in shapes:
        print shape.area()
