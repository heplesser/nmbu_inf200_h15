# -*- coding: utf-8 -*-

"""
RANDU is one of the worst random number generators ever.
It is used here just for illustration.

See http://en.wikipedia.org/wiki/RANDU.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Randu(object):
    """
    Implements RANDU random number generator.

    :note: Do not use for production!
    """

    def seed(self, seed):
        """
        Set seed value of RNG.

        :param seed: seed value
        """

        self._seed = seed

    def rand(self):
        """
        Returns random number in [0, 2**31-1].

        :return: integer random number
        """

        self._seed = (65539 * self._seed) % 2 ** 31
        return self._seed


if __name__ == '__main__':
    randu1 = Randu()
    randu1.seed(10)
    randu2 = Randu()
    randu2.seed(121)
    for _ in range(20):
        print randu1.rand(), randu2.rand()

