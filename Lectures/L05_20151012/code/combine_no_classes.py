# -*- coding: utf-8 -*-

import math

"""
Illustrate Idea 1: Procedural implementation for comparison
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def circle_area(radius):
    return 2 * math.pi * radius**2


def rectangle_area(lower_left, upper_right):
    ll, ur = lower_left, upper_right
    return (ur[0]-ll[0]) * (ur[1]-ll[1])
    
if __name__ == '__main__':
    
    shapes = [{'type': 'Circle', 'center': (0, 0), 'radius': 10},
              {'type': 'Circle', 'center': (1, 1), 'radius':  5},
              {'type': 'Rectangle', 'l_l': (0.5, 0.5), 'u_r': (3, 2)}]
    
    for shape in shapes:
        if shape['type'] == 'Circle':
            print circle_area(shape['radius'])
        elif shape['type'] == 'Rectangle':
            print rectangle_area(shape['l_l'], shape['u_r'])
        else:
            print "Error: unknown shape type"
