# -*- coding: utf-8 -*-

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Friend(object):
    """Represents a friend."""

    def __init__(self, name):
        """
        Create friend.
        :param name: The friend's name
        """
        self.name = name

    def greet(self):
        """Print greeting."""
        print "Hi, " + self.name + "!"


if __name__ == '__main__':
    joe = Friend("Joe")
    jane = Friend("Jane")
    joe.greet()
    jane.greet()
