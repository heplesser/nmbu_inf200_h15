# -*- coding: utf-8 -*-

"""
Illustrate method lookup in a class hierarchy.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class A(object):
    def f(self):
        return "A::f()"

    def g(self):
        return "A::g()"

    def h(self):
        return "A::h()"


class B(A):
    def h(self):
        return "B::h()"


class C(B):
    def g(self):
        return "C::g()"


if __name__ == "__main__":
    a, b, c = A(), B(), C()

    print "a: ", a.f(), a.g(), a.h()
    print "b: ", b.f(), b.g(), b.h()
    print "c: ", c.f(), c.g(), c.h()
