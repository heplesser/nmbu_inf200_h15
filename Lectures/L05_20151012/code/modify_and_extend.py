# -*- coding: utf-8 -*-

"""
Idea 3: Modifying and extending data types.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Member(object):
    def __init__(self, name, number):
        self.name, self.number = name, number

    def display(self):
        print "Member: {0.name} (#{0.number})".format(self)


class Officer(Member):
    def __init__(self, name, number, rank):
        Member.__init__(self, name, number)
        self.rank = rank
        
    def display(self):
        print "{0.rank}: {0.name} (#{0.number})".format(self)


if __name__ == '__main__':
    
    club = [Officer('Joe', 1, 'President'),
            Officer('Jane', 2, 'Treasurer'),
            Member('Jack', 3)]
    
    for person in club:
        person.display()
        
    per = Member('Per', 5)
    per.display()           # "normal" way of calling display method
    Member.display(per)     # explicit way of calling the method
    
    ida = Officer('Ida', 6, 'Webmaster')

    # "normal" way of calling display method displays all info
    ida.display()

    # calling display() from Member class for Officer object
    # will only print normal member information
    Member.display(ida)

    # calling display() from Officer class prints all info
    Officer.display(ida)
