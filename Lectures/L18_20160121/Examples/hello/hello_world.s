	.mod_init_func
	.align 3
	.quad	__GLOBAL__I_main
	.section __TEXT,__textcoal_nt,coalesced,pure_instructions
.globl __ZSt3minImERKT_S2_S2_
	.weak_definition __ZSt3minImERKT_S2_S2_
__ZSt3minImERKT_S2_S2_:
LFB1478:
	pushq	%rbp
LCFI0:
	movq	%rsp, %rbp
LCFI1:
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	jae	L2
	movq	-16(%rbp), %rax
	movq	%rax, -24(%rbp)
	jmp	L4
L2:
	movq	-8(%rbp), %rax
	movq	%rax, -24(%rbp)
L4:
	movq	-24(%rbp), %rax
	leave
	ret
LFE1478:
	.text
__ZStL17__verify_groupingPKcmRKSs:
LFB1407:
	pushq	%rbp
LCFI2:
	movq	%rsp, %rbp
LCFI3:
	pushq	%rbx
LCFI4:
	subq	$88, %rsp
LCFI5:
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdi
	call	__ZNKSs4sizeEv
	decq	%rax
	movq	%rax, -32(%rbp)
	movq	-80(%rbp), %rax
	decq	%rax
	movq	%rax, -48(%rbp)
	leaq	-48(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	call	__ZSt3minImERKT_S2_S2_
	movq	(%rax), %rax
	movq	%rax, -40(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, -56(%rbp)
	movb	$1, -17(%rbp)
	movq	$0, -64(%rbp)
	jmp	L7
L8:
	movq	-56(%rbp), %rsi
	movq	-88(%rbp), %rdi
	call	__ZNKSsixEm
	movzbl	(%rax), %edx
	movq	-64(%rbp), %rax
	addq	-72(%rbp), %rax
	movzbl	(%rax), %eax
	cmpb	%al, %dl
	sete	%al
	movb	%al, -17(%rbp)
	decq	-56(%rbp)
	incq	-64(%rbp)
L7:
	movq	-64(%rbp), %rax
	cmpq	-40(%rbp), %rax
	jae	L11
	cmpb	$0, -17(%rbp)
	jne	L8
	jmp	L11
L12:
	movq	-56(%rbp), %rsi
	movq	-88(%rbp), %rdi
	call	__ZNKSsixEm
	movzbl	(%rax), %edx
	movq	-40(%rbp), %rax
	addq	-72(%rbp), %rax
	movzbl	(%rax), %eax
	cmpb	%al, %dl
	sete	%al
	movb	%al, -17(%rbp)
	decq	-56(%rbp)
L11:
	cmpq	$0, -56(%rbp)
	je	L13
	cmpb	$0, -17(%rbp)
	jne	L12
L13:
	movq	-40(%rbp), %rax
	addq	-72(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jle	L15
	movzbl	-17(%rbp), %ebx
	movq	-88(%rbp), %rdi
	movl	$0, %esi
	call	__ZNKSsixEm
	movzbl	(%rax), %edx
	movq	-40(%rbp), %rax
	addq	-72(%rbp), %rax
	movzbl	(%rax), %eax
	cmpb	%al, %dl
	setle	%al
	movzbl	%al, %eax
	andl	%ebx, %eax
	testl	%eax, %eax
	setne	%al
	movb	%al, -17(%rbp)
L15:
	movzbl	-17(%rbp), %eax
	addq	$88, %rsp
	popq	%rbx
	leave
	ret
LFE1407:
	.cstring
LC0:
	.ascii "Hello, world!\0"
	.text
.globl _main
_main:
LFB1477:
	pushq	%rbp
LCFI6:
	movq	%rsp, %rbp
LCFI7:
	leaq	LC0(%rip), %rsi
	movq	__ZSt4cout@GOTPCREL(%rip), %rdi
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movq	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rsi
	call	__ZNSolsEPFRSoS_E
	movl	$0, %eax
	leave
	ret
LFE1477:
	.section __TEXT,__StaticInit,regular,pure_instructions
__Z41__static_initialization_and_destruction_0ii:
LFB1490:
	pushq	%rbp
LCFI8:
	movq	%rsp, %rbp
LCFI9:
	subq	$16, %rsp
LCFI10:
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	L24
	cmpl	$65535, -8(%rbp)
	jne	L24
	leaq	__ZStL8__ioinit(%rip), %rdi
	call	__ZNSt8ios_base4InitC1Ev
	movq	___dso_handle@GOTPCREL(%rip), %rdx
	movl	$0, %esi
	leaq	___tcf_0(%rip), %rdi
	call	___cxa_atexit
L24:
	leave
	ret
LFE1490:
__GLOBAL__I_main:
LFB1492:
	pushq	%rbp
LCFI11:
	movq	%rsp, %rbp
LCFI12:
	movl	$65535, %esi
	movl	$1, %edi
	call	__Z41__static_initialization_and_destruction_0ii
	leave
	ret
LFE1492:
	.text
___tcf_0:
LFB1491:
	pushq	%rbp
LCFI13:
	movq	%rsp, %rbp
LCFI14:
	subq	$16, %rsp
LCFI15:
	movq	%rdi, -8(%rbp)
	leaq	__ZStL8__ioinit(%rip), %rdi
	call	__ZNSt8ios_base4InitD1Ev
	leave
	ret
LFE1491:
.lcomm __ZStL8__ioinit,1,0
	.section __TEXT,__eh_frame,coalesced,no_toc+strip_static_syms+live_support
EH_frame1:
	.set L$set$0,LECIE1-LSCIE1
	.long L$set$0
LSCIE1:
	.long	0x0
	.byte	0x1
	.ascii "zPR\0"
	.byte	0x1
	.byte	0x78
	.byte	0x10
	.byte	0x6
	.byte	0x9b
	.long	___gxx_personality_v0+4@GOTPCREL
	.byte	0x10
	.byte	0xc
	.byte	0x7
	.byte	0x8
	.byte	0x90
	.byte	0x1
	.align 3
LECIE1:
.globl __ZSt3minImERKT_S2_S2_.eh
	.weak_definition __ZSt3minImERKT_S2_S2_.eh
__ZSt3minImERKT_S2_S2_.eh:
LSFDE1:
	.set L$set$1,LEFDE1-LASFDE1
	.long L$set$1
LASFDE1:
	.long	LASFDE1-EH_frame1
	.quad	LFB1478-.
	.set L$set$2,LFE1478-LFB1478
	.quad L$set$2
	.byte	0x0
	.byte	0x4
	.set L$set$3,LCFI0-LFB1478
	.long L$set$3
	.byte	0xe
	.byte	0x10
	.byte	0x86
	.byte	0x2
	.byte	0x4
	.set L$set$4,LCFI1-LCFI0
	.long L$set$4
	.byte	0xd
	.byte	0x6
	.align 3
LEFDE1:
__ZStL17__verify_groupingPKcmRKSs.eh:
LSFDE3:
	.set L$set$5,LEFDE3-LASFDE3
	.long L$set$5
LASFDE3:
	.long	LASFDE3-EH_frame1
	.quad	LFB1407-.
	.set L$set$6,LFE1407-LFB1407
	.quad L$set$6
	.byte	0x0
	.byte	0x4
	.set L$set$7,LCFI2-LFB1407
	.long L$set$7
	.byte	0xe
	.byte	0x10
	.byte	0x86
	.byte	0x2
	.byte	0x4
	.set L$set$8,LCFI3-LCFI2
	.long L$set$8
	.byte	0xd
	.byte	0x6
	.byte	0x4
	.set L$set$9,LCFI5-LCFI3
	.long L$set$9
	.byte	0x83
	.byte	0x3
	.align 3
LEFDE3:
.globl _main.eh
_main.eh:
LSFDE5:
	.set L$set$10,LEFDE5-LASFDE5
	.long L$set$10
LASFDE5:
	.long	LASFDE5-EH_frame1
	.quad	LFB1477-.
	.set L$set$11,LFE1477-LFB1477
	.quad L$set$11
	.byte	0x0
	.byte	0x4
	.set L$set$12,LCFI6-LFB1477
	.long L$set$12
	.byte	0xe
	.byte	0x10
	.byte	0x86
	.byte	0x2
	.byte	0x4
	.set L$set$13,LCFI7-LCFI6
	.long L$set$13
	.byte	0xd
	.byte	0x6
	.align 3
LEFDE5:
__Z41__static_initialization_and_destruction_0ii.eh:
LSFDE7:
	.set L$set$14,LEFDE7-LASFDE7
	.long L$set$14
LASFDE7:
	.long	LASFDE7-EH_frame1
	.quad	LFB1490-.
	.set L$set$15,LFE1490-LFB1490
	.quad L$set$15
	.byte	0x0
	.byte	0x4
	.set L$set$16,LCFI8-LFB1490
	.long L$set$16
	.byte	0xe
	.byte	0x10
	.byte	0x86
	.byte	0x2
	.byte	0x4
	.set L$set$17,LCFI9-LCFI8
	.long L$set$17
	.byte	0xd
	.byte	0x6
	.align 3
LEFDE7:
__GLOBAL__I_main.eh:
LSFDE9:
	.set L$set$18,LEFDE9-LASFDE9
	.long L$set$18
LASFDE9:
	.long	LASFDE9-EH_frame1
	.quad	LFB1492-.
	.set L$set$19,LFE1492-LFB1492
	.quad L$set$19
	.byte	0x0
	.byte	0x4
	.set L$set$20,LCFI11-LFB1492
	.long L$set$20
	.byte	0xe
	.byte	0x10
	.byte	0x86
	.byte	0x2
	.byte	0x4
	.set L$set$21,LCFI12-LCFI11
	.long L$set$21
	.byte	0xd
	.byte	0x6
	.align 3
LEFDE9:
___tcf_0.eh:
LSFDE11:
	.set L$set$22,LEFDE11-LASFDE11
	.long L$set$22
LASFDE11:
	.long	LASFDE11-EH_frame1
	.quad	LFB1491-.
	.set L$set$23,LFE1491-LFB1491
	.quad L$set$23
	.byte	0x0
	.byte	0x4
	.set L$set$24,LCFI13-LFB1491
	.long L$set$24
	.byte	0xe
	.byte	0x10
	.byte	0x86
	.byte	0x2
	.byte	0x4
	.set L$set$25,LCFI14-LCFI13
	.long L$set$25
	.byte	0xd
	.byte	0x6
	.align 3
LEFDE11:
	.constructor
	.destructor
	.align 1
	.subsections_via_symbols
