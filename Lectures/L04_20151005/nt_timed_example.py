
from nose.tools import timed
import time

@timed(0.1)
def test_long_sleep():
    time.sleep(0.2)
    
@timed(0.1)
def test_fast_addition():
    1 + 1