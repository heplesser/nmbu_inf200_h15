# -*- coding: utf-8 -*-

'''
Implements a bacteria model
'''

from .bacteria_workers import _bact_dies, _bact_divides


class Bacteria(object):
    """Bacteria which die and multiply with fixed probabilities."""

    # These parameters are defined at the class level
    p_death = 0.1   #: probability of death per cycle    @IgnorePep8
    p_divide = 0.1  #: probability of cell division per cycle   @IgnorePep8

    @classmethod
    def set_params(cls, new_params):
        """
        Set class parameters.

        :param new_params: 'p_death' and 'p_divide' can be given
        :type new_params: dict
        :raises: ValueError, KeyError
        """

        for key in new_params:
            if key not in ('p_death', 'p_divide'):
                raise KeyError('Invalid parameter name: ' + key)

        if 'p_death' in new_params:
            if not 0 <= new_params['p_death'] <= 1:
                raise ValueError('p_death must be in [0, 1].')
            cls.p_death = new_params['p_death']

        if 'p_divide' in new_params:
            if not 0 <= new_params['p_divide'] <= 1:
                raise ValueError('p_divide must be in [0, 1].')
            cls.p_divide = new_params['p_divide']

    def __init__(self):
        """Create bacterium with age 0."""
        self.age = 0

    def ages(self):
        """Bacterium ages by one cycle."""
        self.age += 1

    def dies(self):
        """
        Decide whether bacteria dies.

        :returns: True if bacteria dies.
        :rtype: bool
        """
        return _bact_dies(self.p_death)

    def divides(self):
        """
        Decide whether bacteria divides.

        :returns: bool -- True if bacteria divides.
        """

        return _bact_divides(self.p_divide)
