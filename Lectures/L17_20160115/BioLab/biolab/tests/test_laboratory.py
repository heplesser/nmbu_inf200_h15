# -*- coding: utf-8 -*-

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

import nose.tools as nt

from ..laboratory import Lab


def test_lab_create():
    ndish = 5
    n_a = 10
    n_b = 20
    l = Lab(ndish, n_a, n_b)
    tot_a, tot_b = l.bacteria_counts()

    nt.assert_equal(tot_a, ndish * n_a)
    nt.assert_equal(tot_b, ndish * n_b)

