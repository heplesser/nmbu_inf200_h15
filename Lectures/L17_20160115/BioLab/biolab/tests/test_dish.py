# -*- coding: utf-8 -*-

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

import nose.tools as nt

from ..dish import Dish
from ..bacteria import Bacteria
from .plmock import FixedValueMethodMocker


def test_dish_create():
    d = Dish(10, 20)
    nt.assert_equal(d.get_num_a(), 10)
    nt.assert_equal(d.get_num_b(), 20)


class TestAgingCalls(object):
    """
    Tests that Dish.aging() makes the correct number of calls
    to Bacteria.ages().

    This test uses a mock method. It needs to be implemented
    as a class, so that we can share information between setup()
    and teardown() methods run before and after each test.

    The test works as follows:

    1. Each method called test_* is run as one test.
    2. Behind the scenes, nosetest runs the test as follows:

       test_obj = TestAgingCalls()
       for each method called test_*
           test_obj.setup()
           test_obj.test_...()
           test_obj.teardown()

    3. The setup() method stores the real ages() method from
       Bacteria.
    4. The test creates a Mocker object and gets its mocking
       method and replaces Bacteria.ages with that method.
    5. Code is executed, using the mock method instead of the
       real ages() method.
    6. We ask the mocker object how often the mock method has
       been called; we could also check with which arguments
       it was called.
    7. The teardown() method replaces the mock method with
       the real ages() method again, so that other tests
       will use the real Bacteria.ages method.
    """

    def setup(self):
        # noinspection PyAttributeOutsideInit
        self.Bacteria_ages_orig = Bacteria.ages

    def teardown(self):
        Bacteria.ages = self.Bacteria_ages_orig

    # noinspection PyMethodMayBeStatic
    def test_dish_ages(self):
        ages_mocker = FixedValueMethodMocker(None)
        Bacteria.ages = ages_mocker.get_method()

        n_a, n_b = 10, 20

        d = Dish(n_a, n_b)
        d.aging()

        nt.assert_equal(ages_mocker.num_calls(), n_a + n_b)

    # noinspection PyMethodMayBeStatic
    def test_zzfoo(self):
        """
        This test is only here to check that the real Bacteria.ages
        method is properly restored by teardown(). The test is called
        zzfoo on the assumption that tests are executed in alphabetical
        order to make sure that it is run after the test using the
        mocking function.
        """
        b = Bacteria()
        b.ages()
        nt.assert_equal(b.age, 1)
