# -*- coding: utf-8 -*-

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

import nose.tools as nt

from ..simulation import Sim


def test_sim_create():
    ndish = 5
    n_a = 10
    n_b = 20

    s = Sim(ndish, n_a, n_b, 12345)
    data = s.run(cycles=0, return_counts=True)
    nt.assert_equal(data.shape, (1, 3))

    c, tot_a, tot_b = data[0, :]
    nt.assert_equal(c, 0)
    nt.assert_equal(tot_a, ndish * n_a)
    nt.assert_equal(tot_b, ndish * n_b)

