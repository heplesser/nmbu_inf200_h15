'''
Functions implementing "heavy lifting" in Cython.
'''

import random


def _bact_dies(double p):
    """
    Returns True if bacterium dies.

    :param p: Probability of death.
    """

    return random.random() < p


def _bact_divides(double p):
    """
    Returns True if bacterium divides.

    :param p: Probability of death.
    """

    return random.random() < p
