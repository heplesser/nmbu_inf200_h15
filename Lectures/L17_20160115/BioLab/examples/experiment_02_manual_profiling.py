'''
Run profiling with cProfile and create graph.

The profiling graph will be stored as PDF file in the directory in which
you run this script.

For this script to work, you need two external programs:

- gprof2dot
- dot

To install those:

gprof2dot
---------

pip install gprof2dot

dot
---

This belongs to a package called GraphViz, which you can download and
install from

    http://www.graphviz.org
'''

import cProfile
import pstats
import subprocess

from biolab.simulation import Sim

_GPROF2DOT_BINARY = r'gprof2dot'
_DOT_BINARY = r'dot'

if __name__ == '__main__':

    prof_file = 'biolab.prof'
    dot_file = prof_file + '.dot'
    pdf_file = prof_file + '.pdf'

    # Run simulation under profiler control, write data to prof_file
    cProfile.run('Sim(10, 1000, 1000, 12345).run(200, 10)',
                 prof_file)

    # Read profiling data and display as table on screen
    prof = pstats.Stats(prof_file)
    prof.strip_dirs()
    prof.sort_stats('cumulative').print_stats()

    try:
        # Create call graph as dot file using gprof2dot
        subprocess.check_call([_GPROF2DOT_BINARY,
                               '-f', 'pstats',
                               '-o', dot_file,
                               prof_file])
    except subprocess.CalledProcessError as err:
        print "Error while creating dot file:", err

    try:
        # Create PDF file from dot file
        subprocess.check_call([_DOT_BINARY,
                               '-Tpdf',
                               '-o', pdf_file,
                               dot_file])
        print "Profile graph stored as {}".format(pdf_file)

    except subprocess.CalledProcessError as err:
        print "Error while creating PDF file:", err

