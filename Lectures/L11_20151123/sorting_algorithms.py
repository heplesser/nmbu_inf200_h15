# -*- coding: utf-8 -*-

"""
This module provides several sorting algorithms.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def bubble_sort(data):
    """
    Returns sorted copy of data as a list.

    This function uses the bubble sort algorithm to sort the data.

    This implementation is from the example solution EX03, Task C,
    in INF200 at NMBU, Fall term 2014, by Hans E Plesser.

    :param data: any iterable
    :return: a list of the elements of the iterable in sorted order
    """

    sdata = list(data)

    for j in reversed(range(len(sdata))):
        for k in range(j):
            if not sdata[k] <= sdata[k + 1]:
                sdata[k], sdata[k + 1] = sdata[k + 1], sdata[k]

    return sdata


def bubble_sort_swap_equals(data):
    """
    Returns sorted copy of data as a list.

    This function uses the bubble sort algorithm to sort the data.

    This implementation swaps equal values (not optimal).

    This implementation is from the example solution EX03, Task C,
    in INF200 at NMBU, Fall term 2014, by Hans E Plesser.

    :param data: any iterable
    :return: a list of the elements of the iterable in sorted order
    """

    sdata = list(data)

    for j in reversed(range(len(sdata))):
        for k in range(j):
            if not sdata[k] < sdata[k + 1]:
                sdata[k], sdata[k + 1] = sdata[k + 1], sdata[k]

    return sdata


def selection_sort(data):
    """
    Returns sorted copy of data as a list.

    This function uses the selection sort algorithm to sort the data.

    :param data: any iterable
    :return: a list of the elements of the iterable in sorted order
    """

    sdata = list(data)

    n = len(sdata)
    for j in range(0, n - 1):
        min_idx, min_val = j, sdata[j]
        for k in range(j + 1, n):
            if sdata[k] < min_val:
                min_idx, min_val = k, sdata[k]
        sdata[j], sdata[min_idx] = sdata[min_idx], sdata[j]

    return sdata


def selection_sort_immediate_swap(data):
    """
    Returns sorted copy of data as a list.

    This function uses the selection sort algorithm to sort the data.

    While searching for the minimum in data[j+1:], this version swaps
    each new minimal value immediately to data[j]. This leads to shorter
    code but may require more swaps than other implementations.

    :param data: any iterable
    :return: a list of the elements of the iterable in sorted order
    """

    sdata = list(data)

    n = len(sdata)
    for j in range(0, n - 1):
        for k in range(j + 1, n):
            if sdata[k] < sdata[j]:
                sdata[j], sdata[k] = sdata[k], sdata[j]

    return sdata


def selection_sort_idx(data):
    """
    Returns sorted copy of data as a list.

    This function uses the selection sort algorithm to sort the data.

    This version only keeps the index to, but not the value of, the
    minimal value seen so far.

    :param data: any iterable
    :return: a list of the elements of the iterable in sorted order
    """

    sdata = list(data)

    n = len(sdata)
    for j in range(0, n - 1):
        min_idx = j
        for k in range(j + 1, n):
            if sdata[k] < sdata[min_idx]:
                min_idx = k
        sdata[j], sdata[min_idx] = sdata[min_idx], sdata[j]

    return sdata


def quicksort(data):
    """
    Returns sorted copy of data as a list.

    This function uses the quicksort algorithm to sort the data.

    :param data: any iterable
    :return: a list of the elements of the iterable in sorted order
    """

    sdata = list(data)
    _quicksort(sdata, 0, len(sdata))
    return sdata


def _quicksort(data, left, right):
    """
    Recursively sort data[left:right] in place.

    :param data: list or numpy array, will be modified
    :param left: leftmost position to be sorted
    :param right: right-1 is rightmost position to be sorted
    :return: None
    """

    if right - left < 2:
        return

    pivot_idx = _partition(data, left, right)
    _quicksort(data, left, pivot_idx)
    _quicksort(data, pivot_idx + 1, right)


def _partition(data, left, right):
    """
    Partition data[left:right] in place with data[left] as pivot.

    :param data: list or numpy array, will be modified
    :param left: leftmost position to be partitioned
    :param right: right-1 is rightmost position to be partitioned
    :return: index of pivot element after partitioning
    """

    if not left < right:
        return None

    pivot_idx, pivot = left, data[left]

    left += 1  # skip pivot element
    right -= 1  # set to index of last element to be partitioned

    while left < right:

        while left < right and data[left] <= pivot:
            left += 1

        while right > left and data[right] > pivot:
            right -= 1

        if left < right:
            # data[left] is > pivot and data[right] <= pivot => swap
            data[left], data[right] = data[right], data[left]

    assert left == right, "Something is badly wrong: left > right"

    # We now know that:
    # 1. no elements larger pivot in data[:left]
    # 2. no elements smaller or equal pivot in data[right+1:]
    # 3. data is partitioned correctly except for current position

    if data[left] < pivot:
        data[pivot_idx], data[left] = data[left], data[pivot_idx]
        return left
    elif data[left] == pivot:
        return left
    else:
        # data[left] > pivot, thus left-1 must be the new pivot position.
        # Because we ran past left-1, we know that data[left-1] < pivot,
        # so we need to swap values.
        # Because we skipped the pivot element before entering the outer
        # loop above, we know that left >= 1, so left-1 >= 0 is a valid index.
        data[pivot_idx], data[left - 1] = data[left - 1], data[pivot_idx]
        return left - 1


def quicksort_wiki(data):
    """
    Returns sorted copy of data as a list.

    This function uses the quicksort algorithm to sort the data with a
    partitioning function based on the Wikipedia article on Quicksort.

    :param data: any iterable
    :return: a list of the elements of the iterable in sorted order
    """

    sdata = list(data)
    _quicksort_wiki(sdata, 0, len(sdata))
    return sdata


def _quicksort_wiki(data, left, right):
    """
    Recursively sort data[left:right] in place.

    :param data: list or numpy array, will be modified
    :param left: leftmost position to be sorted
    :param right: right-1 is rightmost position to be sorted
    :return: None
    """

    if right - left < 2:
        return

    pivot_idx = _partition_wiki(data, left, right)
    _quicksort_wiki(data, left, pivot_idx)
    _quicksort_wiki(data, pivot_idx + 1, right)


def _partition_wiki(data, left, right):
    """
    Partition data[left:right] in place with data[left] as pivot.

    This function is based on https://en.wikipedia.org/wiki/Quicksort,
    accessed 2014-10-09.

    :param data: list or numpy array, will be modified
    :param left: leftmost position to be partitioned
    :param right: right-1 is rightmost position to be partitioned
    :return: index of pivot element after partitioning
    """

    if not left < right:
        return None

    right -= 1  # convert from upper boundary to uppermost index
    pivot_idx = left
    pivot_val = data[pivot_idx]

    data[pivot_idx], data[right] = data[right], data[pivot_idx]
    store_idx = left

    for i in range(left, right):
        if data[i] < pivot_val:
            data[i], data[store_idx] = data[store_idx], data[i]
            store_idx += 1
        # every time we get here, the following is true:
        # - all values in data[left:store_idx] < pivot_val
        # - all values in data[store_idx:i+1] >= pivot_val
    data[store_idx], data[right] = data[right], data[store_idx]

    return store_idx


def splitsort(data):
    """
    Returns sorted copy of data as a list.

    This function splits the data on a pivot element and sorts the
    split lists recursively.

    :param data: any iterable
    :return: a list of the elements of the iterable in sorted order
    """

    if len(data) < 2:
        return list(data)  # create new list, so that original data
                           # is guaranteed to be untouched

    pivot = data[0]
    smaller = [x for x in data if x < pivot]
    equal = [x for x in data if x == pivot]
    greater = [x for x in data if x > pivot]

    return splitsort(smaller) + equal + splitsort(greater)


if __name__ == '__main__':

    # Import random here since we only need it in main to create test data
    import random

    random.seed(12325424)

    functions_to_test = (bubble_sort, bubble_sort_swap_equals,
                         selection_sort, selection_sort_immediate_swap, selection_sort_idx,
                         quicksort, quicksort_wiki,
                         splitsort)
    test_data_length = 47
    test_data = ([],
                 [1],
                 [2, 1],
                 [2, 3, 1],
                 [1] * test_data_length,
                 range(test_data_length),
                 range(test_data_length, 0, -1),
                 [random.random() for _ in range(test_data_length)])

    tests_passed = 0
    num_tests = 0
    for func in functions_to_test:
        for index, unsorted_data in enumerate(test_data):
            num_tests += 1

            orig_data = unsorted_data[:]  # keep copy to check that data was not changed
            sorted_data = func(unsorted_data)

            if sorted_data != sorted(unsorted_data):
                print "Error---incorrectly sorted: {}(test_data[{}])".format(func.__name__,
                                                                             index)
            elif unsorted_data != orig_data:
                print "Error---data modified     : {}(test_data[{}])".format(func.__name__,
                                                                             index)
            else:
                tests_passed += 1

    if tests_passed == num_tests:
        print "{} of {} tests passed: Congratulations!".format(tests_passed, num_tests)
    else:
        print "{} of {} tests failed: Keep working!".format(num_tests - tests_passed, num_tests)