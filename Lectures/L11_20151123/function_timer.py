# -*- coding: utf-8 -*-

"""
This module provides a class for timing function execution.

The code provided here is based on timeit.py from Python 2.7.8
and the IPython %timeit magic from IPython 2.1.0.
"""

# ----------------------------------------------------------------------
#
# Copyright (C) 2014 Hans Ekkehard Plesser / NMBU
#
# This software is licensed under the modified (3-clause) BSD License
# (http://opensource.org/licenses/BSD-3-Clause).
#
# It is inspired by and patterned on
#
# - IPython.core.magics.extensions.py
#   Copyright (c) 2012 The IPython Development Team; modified BSD License
#   (http://opensource.org/licenses/BSD-3-Clause).
#
# - timeit.py
#   Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
#   2011, 2012, 2013, 2014 Python Software Foundation; All Rights Reserved;
#   Python License v.2 (http://opensource.org/licenses/Python-2.0)
#
# ----------------------------------------------------------------------


__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

import time
import sys
import itertools
import gc
from IPython.core.magics.execution import _format_time as format_time


class FunctionTimer(object):
    """
    Provide services to time execution of a function.
    """

    # System-dependent choice of timer as in timeit.py
    _timer = time.clock if sys.platform == 'win32' else time.time

    def __init__(self, func, args=None):
        """
        :param func: function to be timed
        :param args: argument(s) to be passed to func
        """

        self._func = func
        self._args = args

    def timeit(self, number=1000000):
        """
        Returns time required to run given number of repetitions, in seconds.
        """

        # disable Python garbage collection while running the benchmark
        gc_old = gc.isenabled()
        gc.disable()

        # place all in try-finally to ensure GC is turned on again
        try:
            t0 = self._timer()
            for _ in itertools.repeat(None, number):
                self._func(self._args)
            t1 = self._timer()
        finally:
            if gc_old:
                gc.enable()

        return t1 - t0

    def repeat(self, repeat=3, number=1000000):
        """
        Returns list of results for given number of repetitions.
        """

        return [self.timeit(number) for _ in range(repeat)]

    def auto_timeit(self, repeat=3, quiet=False):
        """
        Returns time *per single execution* in seconds for auto-selected no of executions.
        """

        # ensure we are between 0.2s and 2s, see IPython.core.magic.execution.timeit
        number = 1
        for _ in range(10):
            if self.timeit(number=number) < 0.2:
                number *= 10
            else:
                break

        times = [t / float(number) for t in self.repeat(repeat=repeat, number=number)]

        if not quiet:
            print u'{} loops, best of {}: {} per loop'.format(number, repeat,
                                                              format_time(min(times)))

        return times

