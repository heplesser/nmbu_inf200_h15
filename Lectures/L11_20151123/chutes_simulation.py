# -*- coding: utf-8 -*-

"""
Chutes & Ladders: A fully object-oriented simulation setup.
"""

import random

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Board(object):
    """
    Represents game board.

    This class provides a board of 90 squares (Egmont Libor AS, Oslo, 2001).
    """

    # Default board configuration
    DEFAULT_LADDERS = [(1, 40), (8, 10), (36, 52), (43, 62), (49, 79),
                       (65, 82), (68, 85)]
    DEFAULT_CHUTES = [(24, 5), (33, 3), (42, 30), (56, 37), (64, 27),
                      (74, 12), (87, 70)]
    DEFAULT_GOAL = 90

    def __init__(self, ladders=None, chutes=None, goal=None):
        """
        :param ladders: list of (start, end) tuples representing ladders
        :param chutes: list of (start, end) tuples representing chutes
        :param goal: destination square
        """

        if ladders is None:
            ladders = Board.DEFAULT_LADDERS
        if chutes is None:
            chutes = Board.DEFAULT_CHUTES
        for start, destination in ladders:
            if destination <= start:
                raise ValueError("Invalid ladder {} -> {}".format(start,
                                                                  destination))
        for start, destination in chutes:
            if destination >= start:
                raise ValueError("Invalid chute {} -> {}".format(start,
                                                                 destination))

        self._chutes_and_ladders = {start: end
                                    for start, end in chutes + ladders}
        self._goal = Board.DEFAULT_GOAL if goal is None else goal

        if self._goal <= 0:
            raise ValueError("Invalid goal, must be beyond start.")

    def goal_reached(self, position):
        """
        Returns True if player has reached goal.

        :param position: Player position
        """

        return position >= self._goal

    def position_adjustment(self, position):
        """
        Return change to position due to chute or ladder.

        If the player is not at the start of a chute/ladder, it returns 0.

        :param position: Player position
        :returns: number of fields player must move to get to correct position
        """

        # get() looks up a possible chute or ladder beginning at position.
        # If there is none, get() will return position instead.
        new_position = self._chutes_and_ladders.get(position, position)

        return new_position - position


class Player(object):
    """
    Implements single player.
    """

    def __init__(self, board):
        """
        :param board: board on which player is living
        """
        self._board = board
        self.position = 0
        self.number_of_moves = 0

    def move(self):
        """
        Moves player to new position.
        """

        self.position += random.randint(1, 6)
        self.position += self._board.position_adjustment(self.position)
        self.number_of_moves += 1


class ResilientPlayer(Player):
    """
    Implements a player who makes extra efforts after sliding down.

    At the step after sliding down a slide, this player moves N extra
    squares at the next move.
    """

    def __init__(self, board, extra_steps=1):
        """
        :param extra_steps: number of extra steps on move after sliding down
        """

        Player.__init__(self, board)
        self.extra_steps = extra_steps
        self.went_down_in_last_move = False

    def move(self):
        extra = self.extra_steps if self.went_down_in_last_move else 0
        self.position += random.randint(1, 6) + extra
        adjustment = self._board.position_adjustment(self.position)
        self.position += adjustment
        self.went_down_in_last_move = adjustment < 0
        self.number_of_moves += 1


class LazyPlayer(Player):
    """
    Implements a player who makes a lesser effort after climbing up.

    At the step after climbing a slide, this player moves N fewer
    squares at the next move.
    """

    def __init__(self, board, dropped_steps=1):
        """
        :param dropped_steps: number of steps dropped after climbing up
        """

        Player.__init__(self, board)
        self.dropped_steps = dropped_steps
        self.climbed_in_last_move = False

    def move(self):
        dropped = self.dropped_steps if self.climbed_in_last_move else 0
        self.position += max(0, random.randint(1, 6) - dropped)
        adjustment = self._board.position_adjustment(self.position)
        self.position += adjustment
        self.climbed_in_last_move = adjustment > 0
        self.number_of_moves += 1


class Simulation(object):
    """
    Implements a complete Chutes & Ladders simulation.
    """

    def __init__(self, player_field, board=None, seed=1234567,
                 randomize_players=False):
        """
        :param player_field: list of player classes, one per player to use
        :param board: Board instance (default: standard board)
        :param seed: random generator seed
        :param randomize_players: randomize player order at start of game
        """

        self._player_field = player_field
        self._board = board if board is not None else Board()
        self._results = []
        self._randomize = randomize_players

        random.seed(seed)

    def single_game(self):
        """
        Returns winner type and number of steps for single game.

        :returns: (number_of_steps, winner_class) tuple
        """

        players = [player(self._board) for player in self._player_field]
        if self._randomize:
            random.shuffle(players)

        while True:
            for player in players:
                player.move()
                if self._board.goal_reached(player.position):
                    return player.number_of_moves, player.__class__

    def run_simulation(self, num_games):
        """
        Run a set of games, store results in Simulation.

        If results exist from before, new data will be added to existing data.

        :param num_games: number of games to play
        """

        # Using a loop here allows us to add progress information printout
        for _ in range(num_games):
            self._results.append(self.single_game())

    def players_per_type(self):
        """
        Returns a dict mapping player classes to number of players.
        """

        return {player_type: self._player_field.count(player_type)
                for player_type in frozenset(self._player_field)}

    def winners_per_type(self):
        """
        Returns dict showing number of winners for each type of player.
        """

        winner_types = zip(*self._results)[1]
        return {player_type: winner_types.count(player_type)
                for player_type in frozenset(self._player_field)}

    def durations_per_type(self):
        """
        Returns dict mapping winner type to list of game durations for type.
        """

        return {player_type: [duration for duration, player in self._results
                              if player == player_type]
                for player_type in frozenset(self._player_field)}


if __name__ == '__main__':

    def type_to_name(data):
        """
        Returns data with all type objects in data replaced by __name__ strings.

        Recursively inspect all elements in data and replace those that are
        instances of type with their __name__ string.

        :Note: Inspects list, tuple and dict only.
        """

        if isinstance(data, list):
            return [type_to_name(elem) for elem in data]
        elif isinstance(data, tuple):
            return tuple(type_to_name(elem) for elem in data)
        elif isinstance(data, dict):
            return {type_to_name(key): type_to_name(val)
                    for key, val in data.items()}
        elif isinstance(data, type):
            return data.__name__
        else:
            return data

    print '**** First Simulation: Single player, standard board ****'
    sim = Simulation([Player])
    print type_to_name(sim.single_game())

    sim.run_simulation(10)
    print type_to_name(sim.players_per_type())
    print type_to_name(sim.winners_per_type())
    print type_to_name(sim.durations_per_type())

    print '\n**** Second Simulation: Four players, standard board ****'
    sim = Simulation([Player, Player, LazyPlayer, ResilientPlayer])
    print type_to_name(sim.single_game())

    sim.run_simulation(10)
    print type_to_name(sim.players_per_type())
    print type_to_name(sim.winners_per_type())
    print type_to_name(sim.durations_per_type())

    print '\n**** Third Simulation: Four players, small board ****'
    my_board = Board(ladders=[(3, 10), (5, 8)], chutes=[(9, 2)], goal=20)
    sim = Simulation([Player, Player, LazyPlayer, ResilientPlayer],
                     board=my_board)
    print type_to_name(sim.single_game())

    sim.run_simulation(10)
    print type_to_name(sim.players_per_type())
    print type_to_name(sim.winners_per_type())
    print type_to_name(sim.durations_per_type())
