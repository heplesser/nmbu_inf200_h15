'''
Created on Jan 5, 2013

This file contains a very basic test only for demonstration.

@author: plesser
'''

import nose.tools as nt
import numpy as np

from ..diffsys import DiffSys


def test_create_no_noise():
    ds = DiffSys([1, 1], 0.)
    nt.assert_true(np.array_equal(ds.get_status(), np.zeros([1, 1])))

    ds = DiffSys([2, 3], 0.)
    nt.assert_true(np.array_equal(ds.get_status(), np.zeros([2, 3])))
