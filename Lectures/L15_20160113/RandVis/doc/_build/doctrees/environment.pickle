�(csphinx.environment
BuildEnvironment
qoq}q(Udlfilesqcsphinx.util
FilenameUniqDict
q)�qc__builtin__
set
q]�RqbUappq	NU	_warnfuncq
NUtitlesq}q(Uindexqcdocutils.nodes
title
q)�q}q(U	rawsourceqU U
attributesq}q(Udupnamesq]Uclassesq]Ubackrefsq]Uidsq]Unamesq]uUchildrenq]qcdocutils.nodes
Text
qX#   Welcome to RandVis's documentation!q��q}q(hX#   Welcome to RandVis's documentation!qUparentq hubaUtagnameq!Utitleq"ubUuser_interfaceq#h)�q$}q%(hU h}q&(h]h]h]h]h]uh]q'hX   User Interfaceq(��q)}q*(hX   User Interfaceq+h h$ubah!h"ubUsystemq,h)�q-}q.(hU h}q/(h]h]h]h]h]uh]q0hX   Simulated Systemq1��q2}q3(hX   Simulated Systemq4h h-ubah!h"ubuU
domaindataq5}q6(Ustdq7}q8(Uversionq9K U
anonlabelsq:}q;(Umodindexq<Upy-modindexU �Ugenindexq=h=U �Usearchq>UsearchU �uUlabelsq?}q@(h<Upy-modindexU csphinx.locale
_TranslationProxy
qAcsphinx.locale
mygettext
qBUModule IndexqC��qDhBhC�qE�b�h=h=U hAhBUIndexqF��qGhBhF�qH�b�h>h>U hAhBUSearch PageqI��qJhBhI�qK�b�uUprogoptionsqL}qMUobjectsqN}qOuUc}qP(hN}qQh9K uUpyqR}qS(hN}qT(X   randvis.simulation.DVSimqUh#X   classqV�X"   randvis.diffsys.DiffSys.mean_valueqWh,X   methodqX�X!   randvis.simulation.DVSim.simulateqYh#X   methodqZ�X   randvis.diffsys.DiffSys.updateq[h,X   methodq\�X   randvis.simulationq]h#Umoduleq^�X#   randvis.simulation.DVSim.make_movieq_h#X   methodq`�X   randvis.diffsys.DiffSysqah,X   classqb�X"   randvis.diffsys.DiffSys.get_statusqch,X   methodqd�X   randvis.diffsysqeh,h^�uUmodulesqf}qg(h](h#U U �the(h,U U �tuh9K uUjsqh}qi(hN}qjh9K uUrstqk}ql(hN}qmh9K uUcppqn}qo(hN}qph9K uuUglob_toctreesqqh]�RqrUreread_alwaysqsh]�RqtU
doctreedirquU^/Users/plesser/Courses/INF200/H2013/UMB_INF200_H13/ExamplesJanuary/RandVis/doc/_build/doctreesqvUversioning_conditionqw�U	citationsqx}h9K)UsrcdirqyUN/Users/plesser/Courses/INF200/H2013/UMB_INF200_H13/ExamplesJanuary/RandVis/docqzUconfigq{csphinx.config
Config
q|)�q}}q~(Usource_suffixqU.rstUtexinfo_documentsq�]q�(Uindexq�URandVisq�X   RandVis Documentationq�X   Hans E Plesser, UMBq�h�U One line description of project.UMiscellaneousq�tq�aU	copyrightq�X   2013, Hans E Plesser, UMBUtemplates_pathq�]q�U
_templatesq�aUlatex_documentsq�]q�(h�URandVis.texh�h�Umanualq�tq�aU	overridesq�}Upygments_styleq�Usphinxq�Uexclude_patternsq�]q�U_buildq�aUreleaseq�U0.1q�Uprojectq�X   RandVisUlatex_elementsq�}Uhtmlhelp_basenameq�U
RandVisdoch9h�U
extensionsq�]q�(Usphinx.ext.autodocq�Usphinx.ext.coverageq�Usphinx.ext.mathjaxq�Usphinx.ext.viewcodeq�eUhtml_static_pathq�]q�U_staticq�aU
html_themeq�Udefaultq�U	man_pagesq�]q�(Uindexq�Urandvisq�h�]q�h�aKtq�aU
master_docq�h�Usetupq�NubUmetadataq�}q�(h}h#}h,}uUversionchangesq�}U_viewcode_modulesq�}q�(cdocutils.nodes
reprunicode
q�X   randvis.simulationq���q�}q�bXb  '''
:mod:`randvis.simulation` provides the user interface to the package.

Each simulation is represented by a :class:`DVSim` instance. On each
instance, the :meth:`DVSim.simulate` method can be called as often as
you like to simulate a given number of steps.

The state of the system is visualized as the simulation runs, at intervals
that can be chosen. The graphics can also be saved to file at regular
intervals. By calling :meth:`DVSim.make_movie` after a simulation is complete,
individual graphics files can be combined into an animation.

.. note::
   * This module requires the program ``ffmpeg``,
     available from `<http://ffmpeg.org>`_.
   * You need to set the  :const:`_FFMPEG_BINARY` constant below to the
     location of your ``ffmpeg`` executable.
   * You need to set the :const:`_DEFAULT_FILEBASE` constant below to the
     directory and file-name start you want to use for the graphics output
     files.

Example
--------
::

    sim = DVSim((10, 15), 0.1, 12345, _DEFAULT_FILEBASE)
    sim.simulate(50, 1, 5)
    sim.make_movie()

This code

#. creates a system with a 10x15 matrix, sets the noise level to 0.1,
   the random number generator seed to 12345 and specifies the filename
   for output;
#. performs a simulation of 50 steps, updating the graphics after each
   step and saving a figure after each 5th step;
#. creates a movie from the individual figures saved.

'''

__author__ = "Hans E Plesser, UMB"

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import os
from randvis.diffsys import DiffSys

# update this variable to point to your ffmpeg binary
_FFMPEG_BINARY = '/opt/local/bin/ffmpeg'
# _FFMPEG_BINARY = r'I:\tools\ffmpeg-git-win64-static\bin\ffmpeg'

# update this to the directory and file-name beginning
# for the graphics files
_DEFAULT_GRAPHICS_DIR = os.path.join('..', 'data')
_DEFAULT_GRAPHICS_NAME = 'dv'


class DVSim(object):
    """Provides user interface for simulation, including visualization."""

    def __init__(self, sys_size, noise, seed,
                 img_dir=None, img_name=_DEFAULT_GRAPHICS_NAME,
                 img_fmt='png'):
        """
        :param sys_size:  system size, e.g. (5, 10)
        :type sys_size: (int, int)
        :param noise: noise level
        :type noise: float
        :param seed: random generator seed
        :type seed: int
        :param img_dir: directory for image files; no images if None
        :type img_dir: str
        :param img_name: beginning of name for image files
        :type img_name: str
        :param img_fmt: image file format suffix, default 'png'
        :type img_fmt: str
        """

        np.random.seed(seed)
        self._system = DiffSys(sys_size, noise)
        if img_dir is not None:
            self._img_base = os.path.join(img_dir, img_name)
        else:
            self._img_base = None
        self._img_fmt = img_fmt
        self._step = 0
        self._final_step = None
        self._img_ctr = 0

        # the following will be initialized by _setup_graphics
        self._fig = None
        self._map_ax = None
        self._img_axis = None
        self._mean_ax = None
        self._mean_line = None

    def simulate(self, num_steps, vis_steps=1, img_steps=None):
        """
        Run simulation while visualizing the result.

        :param num_steps: number of simulation steps to execute
        :param vis_steps: interval between visualization updates
        :param img_steps: interval between visualizations saved to files
                          (default: vis_steps)

        .. note:: Image files will be numbered consecutively.
        """

        if img_steps is None:
            img_steps = vis_steps

        self._final_step = self._step + num_steps
        self._setup_graphics()

        while self._step < self._final_step:

            if self._step % vis_steps == 0:
                self._update_graphics()

            if self._step % img_steps == 0:
                self._save_graphics()

            self._system.update()
            self._step += 1

    def make_movie(self):
        """
        Creates MPEG4 movie from visualization images saved.

        .. :note:
            Requires ffmpeg

        The movie is stored as img_base + '.mp4'
        """

        if self._img_base is None:
            raise RuntimeError("No filename defined.")

        try:
            subprocess.check_call([_FFMPEG_BINARY, '-y', '-i',
                                   '{}_%05d.png'.format(self._img_base),
                                   '{}.mp4'.format(self._img_base)])
        except subprocess.CalledProcessError as err:
            print "ERROR: ffmpeg failed:", err

    def _setup_graphics(self):
        """Creates subplots."""

        # create new figure window
        if self._fig is None:
            self._fig = plt.figure()

        # Add left subplot for images created with imshow().
        # We cannot create the actual ImageAxis object before we know
        # the size of the image, so we delay its creation.
        if self._map_ax is None:
            self._map_ax = self._fig.add_subplot(1, 2, 1)
            self._img_axis = None

        # Add right subplot for line graph of mean.
        if self._mean_ax is None:
            self._mean_ax = self._fig.add_subplot(1, 2, 2)
            self._mean_ax.set_ylim(0, 0.02)

        # needs updating on subsequent calls to simulate()
        self._mean_ax.set_xlim(0, self._final_step + 1)

        if self._mean_line is None:
            mean_plot = self._mean_ax.plot(np.arange(0, self._final_step),
                                    np.nan * np.ones(self._final_step))
            self._mean_line = mean_plot[0]
        else:
            xdata, ydata = self._mean_line.get_data()
            xnew = np.arange(xdata[-1] + 1, self._final_step)
            if len(xnew) > 0:
                ynew = np.nan * np.ones_like(xnew)
                self._mean_line.set_data(np.hstack((xdata, xnew)),
                                         np.hstack((ydata, ynew)))

    def _update_system_map(self, sys_map):
        '''Update the 2D-view of the system.'''

        if self._img_axis is not None:
            self._img_axis.set_data(sys_map)
        else:
            self._img_axis = self._map_ax.imshow(sys_map,
                                                 interpolation='nearest',
                                                 vmin=0, vmax=1)
            plt.colorbar(self._img_axis, ax=self._map_ax,
                         orientation='horizontal')

    def _update_mean_graph(self, mean):
        ydata = self._mean_line.get_ydata()
        ydata[self._step] = mean
        self._mean_line.set_ydata(ydata)

    def _update_graphics(self):
        """Updates graphics with current data."""

        self._update_system_map(self._system.get_status())
        self._update_mean_graph(self._system.mean_value())
        plt.draw()

    def _save_graphics(self):
        """Saves graphics to file if file name given."""

        if self._img_base is None:
            return

        plt.savefig('{base}_{num:05d}.{type}'.format(base=self._img_base,
                                                     num=self._img_ctr,
                                                     type=self._img_fmt))
        self._img_ctr += 1


if __name__ == '__main__':

    plt.ion()
    sim = DVSim((10, 15), 0.1, 12345, _DEFAULT_GRAPHICS_DIR)
    sim.simulate(50, 1, 5)
    raw_input('Press ENTER to continue')
    sim.simulate(50, 1, 5)
    sim.make_movie()
q�}q�(X   DVSim._setup_graphicsX   defq�K�K��X   DVSim._save_graphicsX   defq�K�KهX   DVSim.make_movieX   defq�KK��X   DVSim._update_mean_graphX   defq�K�KƇX   DVSim.simulateX   defq�KbK�X   DVSim._update_system_mapX   defq�K�K��X   DVSimq�X   classq�K;KهX   DVSim.__init__X   defq�K>Kb�X   DVSim._update_graphicsX   defq�K�K͇u}q�(X   DVSim.make_movieq�h#X   DVSimq�h#X   DVSim.simulateq�h#u�h�X   randvis.diffsysqǅ�q�}q�bX�  '''
:mod:`randvis.diffsys` implements the simulated system.

The simulated system is very simple. At each step

#. each matrix element is replaced by the average of its own value and that
   of its right-hand neighbor;
#. a random value is added to each matrix element;
#. periodic boundary conditions are used, i.e., the leftmost column in the
   matrix is considered to be the right-hand neighbor of the rightmost column.
'''

__author__ = "Hans E Plesser, UMB"

import numpy as np


class DiffSys(object):
    """Represents the system under simulation."""

    def __init__(self, syssize, noiselevel):
        """
        :param syssize: system size, (rows, columns)
        :param noiselevel: add uniform noise
                           from [-noiselevel/2, noiselevel/2)
        """

        if any(elem < 1 for elem in syssize):
            raise ValueError("System size must be strictly positive.")
        self._rows, self._cols = syssize

        if noiselevel < 0:
            raise ValueError("Noise level cannot be negative.")

        self._noise = noiselevel
        self._system = self._noise_matrix()

        # This index allows adding the value of the right neighbor
        # with periodic boundary conditions when uses as
        # self._system[:, _idx] += self._system
        self._idx = np.arange(-1, self._cols - 1)

    def _noise_matrix(self):
        """Random matrix of system size values in [-noise/2, noise/2)"""

        return self._noise * (np.random.rand(self._rows, self._cols)
                              - 0.5)

    def update(self):
        """Advances system state."""

        self._system[:, self._idx] += self._system
        self._system *= 0.5
        self._system += self._noise_matrix()

    def mean_value(self):
        """Returns mean value of system elements."""

        return self._system.mean()

    def get_status(self):
        """Returns full data matrix."""

        return self._system
q�}q�(X   DiffSys.updateX   defq�K1K8�X   DiffSys._noise_matrixX   defq�K+K1�X   DiffSysq�X   classq�KKA�X   DiffSys.get_statusX   defq�K=KA�X   DiffSys.__init__X   defq�KK+�X   DiffSys.mean_valueX   defq�K8K=�u}q�(X   DiffSys.get_statusq�h,X   DiffSysq�h,X   DiffSys.updateq�h,X   DiffSys.mean_valueq�h,u�uUtoc_num_entriesq�}q�(hKh#Kh,KuUimagesq�h)�q�h]�Rq�bUnumbered_toctreesq�h]�Rq�U
found_docsq�h]q�(hh#h,e�Rq�U
longtitlesq�}q�(hhh#h$h,h-uUdependenciesq�}q�(h#h]q�U../randvis/simulation.pyq�a�Rq�h,h]q�U../randvis/diffsys.pyq�a�Rq�uUtoctree_includesq�}q�h]q�(X   user_interfaceq�X   systemq�esU	temp_dataq�}Utocsq�}q�(hcdocutils.nodes
bullet_list
q�)�q�}q�(hU h}q�(h]h]h]h]h]uh]q�(cdocutils.nodes
list_item
q�)�q�}q�(hU h}q�(h]h]h]h]h]uh h�h]q�(csphinx.addnodes
compact_paragraph
q�)�q�}r   (hU h}r  (h]h]h]h]h]uh h�h]r  cdocutils.nodes
reference
r  )�r  }r  (hU h}r  (U
anchornameU Urefurihh]h]h]h]h]Uinternal�uh h�h]r  hX#   Welcome to RandVis's documentation!r  ��r	  }r
  (hhh j  ubah!U	referencer  ubah!Ucompact_paragraphr  ubh�)�r  }r  (hU h}r  (h]h]h]h]h]uh h�h]r  csphinx.addnodes
toctree
r  )�r  }r  (hU h}r  (UnumberedK UparenthU
titlesonly�Uglob�h]h]h]h]h]Uentries]r  (Nh�r  Nh��r  eUhidden�Uincludefiles]r  (h�h�eUmaxdepthKuh j  h]h!Utoctreer  ubah!Ubullet_listr  ubeh!U	list_itemr  ubh�)�r  }r  (hU h}r  (h]h]h]h]h]uh h�h]r  h�)�r   }r!  (hU h}r"  (h]h]h]h]h]uh j  h]r#  j  )�r$  }r%  (hU h}r&  (U
anchornameU#indices-and-tablesUrefurihh]h]h]h]h]Uinternal�uh j   h]r'  hX   Indices and tablesr(  ��r)  }r*  (hX   Indices and tablesh j$  ubah!j  ubah!j  ubah!j  ubeh!j  ubh#h�)�r+  }r,  (hU h}r-  (h]h]h]h]h]uh]r.  h�)�r/  }r0  (hU h}r1  (h]h]h]h]h]uh j+  h]r2  (h�)�r3  }r4  (hU h}r5  (h]h]h]h]h]uh j/  h]r6  j  )�r7  }r8  (hU h}r9  (U
anchornameU Urefurih#h]h]h]h]h]Uinternal�uh j3  h]r:  hX   User Interfacer;  ��r<  }r=  (hh+h j7  ubah!j  ubah!j  ubh�)�r>  }r?  (hU h}r@  (h]h]h]h]h]uh j/  h]rA  h�)�rB  }rC  (hU h}rD  (h]h]h]h]h]uh j>  h]rE  h�)�rF  }rG  (hU h}rH  (h]h]h]h]h]uh jB  h]rI  j  )�rJ  }rK  (hU h}rL  (U
anchornameU#exampleUrefurih#h]h]h]h]h]Uinternal�uh jF  h]rM  hX   ExamplerN  ��rO  }rP  (hX   ExamplerQ  h jJ  ubah!j  ubah!j  ubah!j  ubah!j  ubeh!j  ubah!j  ubh,h�)�rR  }rS  (hU h}rT  (h]h]h]h]h]uh]rU  h�)�rV  }rW  (hU h}rX  (h]h]h]h]h]uh jR  h]rY  h�)�rZ  }r[  (hU h}r\  (h]h]h]h]h]uh jV  h]r]  j  )�r^  }r_  (hU h}r`  (U
anchornameU Urefurih,h]h]h]h]h]Uinternal�uh jZ  h]ra  hX   Simulated Systemrb  ��rc  }rd  (hh4h j^  ubah!j  ubah!j  ubah!j  ubah!j  ubuUindexentriesre  }rf  (h]h#]rg  ((Usinglerh  X   randvis.simulation (module)X   module-randvis.simulationU tri  (jh  X#   DVSim (class in randvis.simulation)hUU trj  (jh  X.   make_movie() (randvis.simulation.DVSim method)h_U trk  (jh  X,   simulate() (randvis.simulation.DVSim method)hYU trl  eh,]rm  ((jh  X   randvis.diffsys (module)X   module-randvis.diffsysU trn  (jh  X"   DiffSys (class in randvis.diffsys)haU tro  (jh  X-   get_status() (randvis.diffsys.DiffSys method)hcU trp  (jh  X-   mean_value() (randvis.diffsys.DiffSys method)hWU trq  (jh  X)   update() (randvis.diffsys.DiffSys method)h[U trr  euUall_docsrs  }rt  (hGAԴ�Ww�h#GAԴ�s�h,GAԴ�` �uUsettingsru  }rv  (Ucloak_email_addressesrw  �Utrim_footnote_reference_spacerx  �U
halt_levelry  KUsectsubtitle_xformrz  �Uembed_stylesheetr{  �Upep_base_urlr|  Uhttp://www.python.org/dev/peps/r}  Udoctitle_xformr~  �Uwarning_streamr  csphinx.util.nodes
WarningStream
r�  )�r�  }r�  (U_rer�  cre
_compile
r�  U+\((DEBUG|INFO|WARNING|ERROR|SEVERE)/[0-4]\)r�  K �Rr�  Uwarnfuncr�  NubUenvr�  hUrfc_base_urlr�  Uhttp://tools.ietf.org/html/r�  Ugettext_compactr�  �Uinput_encodingr�  U	utf-8-sigr�  uUfiles_to_rebuildr�  }r�  (h�h]r�  ha�Rr�  h�h]r�  ha�Rr�  uUtoc_secnumbersr�  }U_nitpick_ignorer�  h]�Rr�  ub.