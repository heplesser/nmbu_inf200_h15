-------
RandVis
-------

This package contains a small, meaningless simulation for demonstration 
purposes. It is mainly meant as an illustration for how to package Python 
projects and how to use Sphinx to generate documentation.

To install, run::
    python setup.py install          (installs to default location for all users)
    python setup.py install --user   (installs to default location for user)
    python setup.py install --prefix=... (installs to given path)

Hans E Plesser / UMB, January 2013
