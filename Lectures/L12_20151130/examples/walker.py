# -*- coding: utf-8 -*-

import random

"""
A reduced Walker version for profiling experiments.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Walker(object):
    def __init__(self, start, home):
        self.x = start
        self.home = home
        self.steps = 0

    def get_position(self):
        return self.x

    def get_steps(self):
        return self.steps

    def is_at_home(self):
        return self.x == self.home

    def move(self):
        self.x += 2 * random.randint(0, 1) - 1
        self.steps += 1

    def go_home(self):
        while not self.is_at_home():
            self.move()
        return self.get_steps()


if __name__ == '__main__':

    random.seed(12345)
    print Walker(0, 50).go_home()
