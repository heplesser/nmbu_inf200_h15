# -*- coding: utf-8 -*-

from clsim.player import Player
from clsim.simulation import Simulation
from clsim.board import BoardInterpolatedSearch as BoardType

"""
Script to profile interpolation-based search.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

Simulation([Player], seed=12345, board=BoardType(scale=5000)).single_game()

