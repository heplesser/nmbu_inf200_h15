# -*- coding: utf-8 -*-

import nose.tools as nt
from . import board

"""
Tests for board class.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def get_board_classes():
    return [cls for cls in board.__dict__.values()
            if isinstance(cls, type) and issubclass(cls, board.Board)]


def test_bad_goal():
    nt.assert_raises(ValueError, board.Board, goal=-1)


def test_bad_ladder():
    nt.assert_raises(ValueError, board.Board, ladders=[(10, 10)])
    nt.assert_raises(ValueError, board.Board, ladders=[(10, 3)])


def test_bad_chute():
    nt.assert_raises(ValueError, board.Board, chutes=[(10, 10)])
    nt.assert_raises(ValueError, board.Board, chutes=[(3, 10)])


def test_goal_false_positives():
    b = board.Board(goal=100)
    for n in range(100):
        nt.assert_false(b.goal_reached(n))


def test_goal_false_negatives():
    b = board.Board(goal=100)
    for n in range(100, 120):
        nt.assert_true(b.goal_reached(n))


def test_plain_board():
    for board_type in get_board_classes():
        yield check_plain_board, board_type


def check_plain_board(board_type):
    b = board_type(ladders=[], chutes=[], goal=100)
    for n in range(1, 102):
        nt.assert_equal(b.position_adjustment(n), 0)


def test_nontrivial_board():
    nt.assert_greater(len(get_board_classes()), 0)

    for board_type in get_board_classes():
        yield check_nontrivial_board, board_type


def check_nontrivial_board(board_type):
    ladders = [(1, 40), (8, 10), (36, 52), (43, 62), (49, 79),
               (65, 82), (68, 85)]
    chutes = [(24, 5), (33, 3), (42, 30), (56, 37), (64, 27),
              (74, 12), (87, 70)]
    goal = 100
    cldict = dict(ladders + chutes)
    b = board_type(ladders=ladders, chutes=chutes, goal=goal)

    for n in range(1, goal+2):
        if n in cldict:
            nt.assert_equal(b.position_adjustment(n), cldict[n]-n)
        else:
            nt.assert_equal(b.position_adjustment(n), 0)


# noinspection PyProtectedMember
def test_scaled_board():
    scale = 5
    base = board.Board()
    scaled = board.Board(scale=scale)

    nt.assert_equal(scale * base._goal, scaled._goal)

    bl, bc = base._ladders, base._chutes
    sl, sc = scaled._ladders, scaled._chutes

    nt.assert_equal(scale * len(bl), len(sl))
    nt.assert_equal(scale * len(bc), len(sc))


