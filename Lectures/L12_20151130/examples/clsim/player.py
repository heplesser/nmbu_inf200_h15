# -*- coding: utf-8 -*-

"""
Player classes for Chutes & Ladders games.
"""

import random

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Player(object):
    """
    Implements single player.
    """

    def __init__(self, board):
        """
        :param board: board on which player is living
        """
        self._board = board
        self._position = 0
        self.number_of_moves = 0

    @property
    def position(self):
        """Player's current position."""
        return self._position

    def _move(self, additional_steps=0):
        """
        Helper function performing move, taking extra steps into account and
        reporting adjustment.

        :param additional_steps: add to result of die cast
        :returns: adjustment as integer number of fields (<0: slid down)
        """

        self._position += max(0, random.randint(1, 6) + additional_steps)
        adjustment = self._board.position_adjustment(self._position)
        self._position += adjustment
        self.number_of_moves += 1
        return adjustment

    def move(self):
        """
        Moves player to new position.
        """

        self._move()


class ResilientPlayer(Player):
    """
    Implements a player who makes extra efforts after sliding down.

    At the step after sliding down a slide, this player moves N extra
    squares at the next move.
    """

    def __init__(self, board, extra_steps=1):
        """
        :param extra_steps: number of extra steps on move after sliding down
        """

        Player.__init__(self, board)
        self.extra_steps = extra_steps
        self.went_down_in_last_move = False

    def move(self):
        extra = self.extra_steps if self.went_down_in_last_move else 0
        adjustment = self._move(extra)
        self.went_down_in_last_move = adjustment < 0


class LazyPlayer(Player):
    """
    Implements a player who makes a lesser effort after climbing up.

    At the step after climbing a slide, this player moves N fewer
    squares at the next move.
    """

    def __init__(self, board, dropped_steps=1):
        """
        :param dropped_steps: number of steps dropped after climbing up
        """

        Player.__init__(self, board)
        self.dropped_steps = dropped_steps
        self.climbed_in_last_move = False

    def move(self):
        # number of extra steps must be negative, since we drop steps
        extra = -self.dropped_steps if self.climbed_in_last_move else 0
        adjustment = self._move(extra)
        self.climbed_in_last_move = adjustment > 0
