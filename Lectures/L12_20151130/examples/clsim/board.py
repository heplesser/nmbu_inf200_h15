# -*- coding: utf-8 -*-

"""
Board classes for Chutes & Ladders simulations.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Board(object):
    """
    Represents game board.

    This class provides a board of 90 squares (Egmont Libor AS, Oslo, 2001).
    """

    # Default board configuration
    DEFAULT_LADDERS = [(1, 40), (8, 10), (36, 52), (43, 62), (49, 79),
                       (65, 82), (68, 85)]
    DEFAULT_CHUTES = [(24, 5), (33, 3), (42, 30), (56, 37), (64, 27),
                      (74, 12), (87, 70)]
    DEFAULT_GOAL = 90

    def __init__(self, ladders=None, chutes=None, goal=None, scale=1):
        """
        :param ladders: list of (start, end) tuples representing ladders
        :param chutes: list of (start, end) tuples representing chutes
        :param goal: destination square
        :param scale: the board is repeated this number of times (integer)
        """

        self._ladders = Board.DEFAULT_LADDERS if ladders is None else ladders
        self._chutes = Board.DEFAULT_CHUTES if chutes is None else chutes
        self._goal = Board.DEFAULT_GOAL if goal is None else goal

        if self._goal <= 0:
            raise ValueError("Invalid goal, must be beyond start.")

        for start, destination in self._ladders:
            if destination <= start:
                raise ValueError("Invalid ladder {} -> {}".format(start,
                                                                  destination))

        for start, destination in self._chutes:
            if destination >= start:
                raise ValueError("Invalid chute {} -> {}".format(start,
                                                                 destination))

        if not (scale >= 1 and scale % 1 == 0):
            raise ValueError("Scale must be positive integer.")

        if scale > 1:
            blocksz = self._goal
            self._goal *= scale
            self._ladders = [(start + n * blocksz, dest + n * blocksz)
                             for start, dest in self._ladders
                             for n in range(scale)]
            self._chutes = [(start + n * blocksz, dest + n * blocksz)
                            for start, dest in self._chutes
                            for n in range(scale)]

    def goal_reached(self, position):
        """
        Returns True if position is at or beyond goal.

        :param position: player position
        """

        return position >= self._goal

    def position_adjustment(self, position):
        """
        Return change to position due to chute or ladder.

        If the position is not at the start of a chute/ladder, it returns 0.

        :param position: Position on the board
        :returns: fields player has to move to get to correct position
        """

        new_position = position
        for start, destination in self._chutes + self._ladders:
            if position == start:
                new_position = destination
                break

        return new_position - position


class BoardBinarySearch(Board):
    """
    Board variant using binary search to find chutes and ladders.
    """

    def __init__(self, ladders=None, chutes=None, goal=None, scale=1):
        super(BoardBinarySearch, self).__init__(ladders, chutes,
                                                goal, scale)

        # binary search requires sorted data
        self._chutes_and_ladders = sorted(self._chutes + self._ladders)

    def position_adjustment(self, position):
        new_position = position
        cnl = self._chutes_and_ladders

        l, r = 0, len(cnl)
        k = (l + r) / 2

        while l != r and cnl[k][0] != position:
            if position < cnl[k][0]:
                r = k
            else:
                l = k + 1
            k = (l + r) / 2
        if l != r:  # we left while because cnl[k][0] == position
            new_position = cnl[k][1]

        return new_position - position


class BoardDictSearch(Board):
    """
    Board variant using dictionary search to find chutes and ladders.
    """

    def __init__(self, ladders=None, chutes=None, goal=None, scale=1):
        super(BoardDictSearch, self).__init__(ladders, chutes,
                                              goal, scale)

        # store in dictionary
        self._chutes_and_ladders = dict(self._chutes + self._ladders)

    def position_adjustment(self, position):
        return self._chutes_and_ladders.get(position, position) - position


class BoardInterpolatedSearch(Board):
    """
    Board variant finding starting point for search by interpolation.
    """

    def __init__(self, ladders=None, chutes=None, goal=None, scale=1):
        super(BoardInterpolatedSearch, self).__init__(ladders, chutes,
                                                      goal, scale)

        # interpolated search requires sorted data
        self._chutes_and_ladders = sorted(self._chutes + self._ladders)

    def position_adjustment(self, position):
        # code below depends on at least one element in cnl list
        if not self._chutes_and_ladders:
            return 0

        # Use linear interpolation into array
        cnl = self._chutes_and_ladders
        n_cnl = len(cnl)
        guess = (position * n_cnl) / self._goal
        guess = min(guess, n_cnl-1)    # make sure guess is valid index into cnl

        if position < cnl[guess][0]:
            while guess > 0 and position < cnl[guess][0]:
                guess -= 1
        else:
            while guess < n_cnl-1 and position > cnl[guess][0]:
                guess += 1

        if cnl[guess][0] == position:
            return cnl[guess][1] - position
        else:
            return 0
