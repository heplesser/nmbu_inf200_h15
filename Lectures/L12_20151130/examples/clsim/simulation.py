# -*- coding: utf-8 -*-

"""
Simulation class for Chutes & Ladders.
"""

import random
from .board import Board

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Simulation(object):
    """
    Implements a complete Chutes & Ladders simulation.
    """

    def __init__(self, player_field, board=None, seed=1234567,
                 randomize_field=False):
        """
        :param player_field: list of player classes, one player per entry
        :param board: Board instance defining playing field
        :param seed: random generator seed
        :param randomize_field: randomize order of players before each game
        """

        self._player_field = player_field
        self._board = board if board is not None else Board()
        self._results = []
        self._randomize = randomize_field

        random.seed(seed)

    def single_game(self):
        """
        Run single game and return winner type and number of steps.

        :returns: (number_of_steps, winner_class) tuple
        """

        players = [player(self._board) for player in self._player_field]
        if self._randomize:
            random.shuffle(players)

        while True:
            for player in players:
                player.move()
                if self._board.goal_reached(player.position):
                    return player.number_of_moves, player.__class__

    def run_simulation(self, num_games):
        """
        Run a set of games, store results in Simulation.

        If results exist from before, new data will be added to existing data.

        :param num_games: number of games to play
        """

        # Using a loop here allows us to add progress information printout
        for _ in range(num_games):
            self._results.append(self.single_game())

    def players_per_type(self):
        """
        Returns a dict mapping player classes to number of players.
        """

        return {player_type: self._player_field.count(player_type)
                for player_type in frozenset(self._player_field)}

    def winners_per_type(self):
        """
        Returns dict showing number of winners for each type of player.
        """

        winner_types = zip(*self._results)[1]
        return {player_type: winner_types.count(player_type)
                for player_type in frozenset(self._player_field)}

    def durations_per_type(self):
        """
        Returns dict mapping winner type to all game durations for that type.
        """

        return {player_type: [duration for duration, player in self._results
                              if player == player_type]
                for player_type in frozenset(self._player_field)}
